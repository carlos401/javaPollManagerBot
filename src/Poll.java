/*
    ldapSyncBot
    Copyright (C) 2018  CrabTech

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import javafx.util.Pair;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class Poll {
    boolean finished=false;
    boolean is_schedule_poll=false;
    long chat_creation=0;
    int id = 0;
    int id_creator = 0;
    boolean limit_option_selection=false;
    String creator = "";
    String name = "";
    String question = "";
    GregorianCalendar limit ;
    LinkedList<String> options = new LinkedList<>();
    LinkedList<Integer> limit_votes=null;
    HashMap<Long,Integer> groups = new HashMap<>();
    Semaphore semaphore = new Semaphore(1);
    LinkedList<Integer>[] votos ;


    Poll(int id_creator, String creator) {
        this.id_creator = id_creator;
        this.creator = creator;
    }

    public int setLimit(String limit) {
        int error = 0;

        GregorianCalendar fecha = null;
        GregorianCalendar fecha_2=new GregorianCalendar();
        fecha_2.setTime(new Date(Long.MAX_VALUE));
        String pattern = "^[0-9]{1,2}\\/[0-9]{1,2}\\/[0-9]{1,4} (?:(?:2[0-3])|(?:[0-1]?[0-9])):(?:[0-5]?[0-9])$";
        if (limit.matches(pattern)) {
            int day = Integer.parseInt(limit.split(" ")[0].split("/")[0]);
            int month = Integer.parseInt(limit.split(" ")[0].split("/")[1]);
            int year = Integer.parseInt(limit.split(" ")[0].split("/")[2]);
            int hour = Integer.parseInt(limit.split(" ")[1].split(":")[0]);
            int minute = Integer.parseInt(limit.split(" ")[1].split(":")[1]);
            try {
                fecha = new GregorianCalendar(year, month - 1, day, hour, minute);
                if(is_schedule_poll){

                    for(int c=0;c<options.size();c++)
                    {
                        int day_2 = Integer.parseInt(limit.split(" ")[0].split("/")[0]);
                        int month_2 = Integer.parseInt(limit.split(" ")[0].split("/")[1]);
                        int year_2 = Integer.parseInt(limit.split(" ")[0].split("/")[2]);
                        int hour_2 = Integer.parseInt(limit.split(" ")[1].split(":")[0]);
                        int minute_2 = Integer.parseInt(limit.split(" ")[1].split(":")[1]);
                        GregorianCalendar ge= new GregorianCalendar(year_2, month_2 - 1, day_2, hour_2, minute_2);
                        if(ge.compareTo(fecha_2)<0)
                        {
                            fecha_2=ge;
                        }
                    }

                }
            } catch (Exception e) {
                error = 1;
            }
            GregorianCalendar compare = (GregorianCalendar) GregorianCalendar.getInstance();
            compare.add(Calendar.MINUTE, 1);
            if(is_schedule_poll&&fecha.compareTo(fecha_2)<0)
            {
                error=2;
            }
            if (!(fecha.compareTo(compare) >= 0)) {
                error = 3;
            }
            if(is_schedule_poll&&fecha_2.compareTo(compare)<0)
            {
                error=4;
            }
            this.limit=fecha;
        }
        else {
            error=1;
        }
        return error;
    }

    public String getStringLimit()
    {
        SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        return fmt.format(limit.getTime());
    }
    public void add_options(String option) {
        options.add(option);
    }
}