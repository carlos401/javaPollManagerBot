/*
    ldapSyncBot
    Copyright (C) 2018  CrabTech

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import org.glassfish.grizzly.utils.EchoFilter;
import org.telegram.telegrambots.api.methods.AnswerCallbackQuery;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.methods.send.SendPhoto;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.util.*;
import java.util.concurrent.Semaphore;

public class Bot extends TelegramLongPollingBot {
    class MessageThread implements Runnable
    {
        Update update=null;
        Bot bot=null;
        MessageThread(Update update,Bot father)
        {
            this.update=update;
            this.bot=father;
        }
        @Override
        public void run() {
            if(update.hasMessage()&&update.getMessage().hasText())
            {
                if(update.getMessage().getChat().isUserChat())
                {
                    if(botsPrivados.containsKey(update.getMessage().getChatId()))
                    {
                        botsPrivados.get(update.getMessage().getChatId()).handle(update);
                    }
                    else
                    {
                        PrivateBot pb=new PrivateBot(bot);
                        botsPrivados.put(update.getMessage().getChatId(),pb);
                        pb.handle(update);
                    }
                }
                else{
                    if(botsGrupales.containsKey(update.getMessage().getChatId()))
                    {
                        botsGrupales.get(update.getMessage().getChatId()).handle(update);
                    }
                    else
                    {
                        GroupBot pb=new GroupBot(bot);
                        botsGrupales.put(update.getMessage().getChatId(),pb);
                        pb.handle(update);
                    }
                }
            }
            if (update.hasCallbackQuery())
            {
                if(update.getCallbackQuery().getMessage().getChat().isUserChat())
                {
                    if(botsPrivados.containsKey(update.getCallbackQuery().getMessage().getChatId()))
                    {
                        botsPrivados.get(update.getCallbackQuery().getMessage().getChatId()).handle_line_button(update);
                    }
                    else
                    {
                        PrivateBot pb=new PrivateBot(bot);
                        botsPrivados.put(update.getCallbackQuery().getMessage().getChatId(),pb);
                        pb.handle_line_button(update);
                    }
                }
                else{
                    if(botsGrupales.containsKey(update.getCallbackQuery().getMessage().getChat()))
                    {
                        botsGrupales.get(update.getCallbackQuery().getMessage().getChat()).handle(update);
                    }
                    else
                    {
                        GroupBot pb=new GroupBot(bot);
                        botsGrupales.put(update.getCallbackQuery().getMessage().getChatId(),pb);
                        pb.handle_line_button(update);
                    }
                }
            }
        }
    }

    int count_polls=0;
    HashMap<Integer, Poll> encuestas = new HashMap<>();
    Semaphore semaphore=new Semaphore(1);
    HashMap<Long, PrivateBot> botsPrivados= new HashMap<>();
    HashMap<Long, GroupBot> botsGrupales= new HashMap<>();
    HashMap<Integer,String> usuarios=new HashMap<>();
    @Override
    public void onUpdateReceived(Update update) {
        Thread mThread= new Thread(new MessageThread(update,this));
        mThread.start();
    }
    @Override
    public String getBotUsername() {
        return null;
    }

    @Override
    public String getBotToken() {
        return "561044598:AAEnh8WYJ9VJViQ83qbHmSJVp5F3zqMQUcI";
    }


}
