/*
    ldapSyncBot
    Copyright (C) 2018  CrabTech

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.util.*;

import org.telegram.telegrambots.api.methods.AnswerCallbackQuery;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardRemove;

public class GroupBot {
    private Bot bot;

    public GroupBot(Bot iomanager) {
        bot = iomanager;
    }

    public void handle(Update update) {
        if (update.getMessage().getText().contains("shareevent")) {
                shareEventMsg(update);
            } else {

            }
    }

    public void send_message(Update update, String message, ReplyKeyboardMarkup keyboardMarkup) {
        if (keyboardMarkup == null) {
            SendMessage reply = new SendMessage()
                    .setChatId(update.getMessage().getChatId())
                    .setText(message).setReplyMarkup(new ReplyKeyboardRemove());
            try {
                bot.execute(reply);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            SendMessage reply = new SendMessage()
                    .setChatId(update.getMessage().getChatId())
                    .setText(message).setReplyMarkup(keyboardMarkup);
            try {
                bot.execute(reply);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void answerCallback(Update update, String text) {
        AnswerCallbackQuery scbq = new AnswerCallbackQuery()
                .setCallbackQueryId(update.getCallbackQuery().getId())
                .setText(text);
        try {
            bot.execute(scbq);
        } catch (Exception e) {
        }
    }

    public void handle_line_button(Update update) {
        String name = "Sin nombre usuario";
        if (update.getCallbackQuery().getFrom().getFirstName() != null) {
            name = update.getCallbackQuery().getFrom().getFirstName();
        }
        if (update.getCallbackQuery().getFrom().getFirstName() != null) {
            name += update.getCallbackQuery().getFrom().getLastName();
        }
        if (bot.usuarios.containsKey(update.getCallbackQuery().getFrom().getFirstName())) {
            bot.usuarios.replace(update.getCallbackQuery().getFrom().getId(), name);
        } else {
            bot.usuarios.put(update.getCallbackQuery().getFrom().getId(), name);
        }

        String query_data = update.getCallbackQuery().getData();
        int from_id = update.getCallbackQuery().getFrom().getId();
        int number = Integer.parseInt(query_data.split("_")[0]);
        int event = Integer.parseInt(query_data.split("_")[1]);
        Poll poll = null;
        try {
            bot.semaphore.acquire();
            if (bot.encuestas.containsKey(event)) {
                poll = bot.encuestas.get(event);
            }
            bot.semaphore.release();
        } catch (Exception e) {
        }
        if (poll != null&&!poll.finished) {
            try {
                poll.semaphore.acquire();
                try {
                    if (poll.votos[number].contains(from_id)) {
                        poll.votos[number].remove(poll.votos[number].indexOf(from_id));
                        answerCallback(update, "Opción " + (number + 1) + " deseleccionada");
                    } else {
                        if (poll.limit_option_selection == true) {

                            if (poll.votos[number].size() < poll.limit_votes.get(number)) {
                                answerCallback(update, "Opción " + (number + 1) + " seleccionada");
                                poll.votos[number].add(from_id);
                            } else {
                                answerCallback(update, "Opción " + (number + 1) + " ya tiene el número máximo de selecciones");
                            }
                        } else {
                            answerCallback(update, "Opción " + (number + 1) + " seleccionada");
                            poll.votos[number].add(from_id);
                        }
                    }

                    InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
                    List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
                    String text = poll.creator + " ha compartido una encuesta.\nNombre: "
                            + poll.name + "\nInterrogante: " + poll.question + "\nLista de opciones:\n";
                    for (int c = 0; c < poll.options.size(); c++) {
                        text += (c + 1) + "." + poll.options.get(c);
                        if (poll.limit_option_selection) {
                            text += " (" + poll.votos[c].size() + "/" + poll.limit_votes.get(c) + ")\n";
                        } else {
                            text += " (" + poll.votos[c].size() + ")\n";
                        }
                        List<InlineKeyboardButton> rowInline = new ArrayList<>();
                        rowInline.add(new InlineKeyboardButton()
                                .setText((c + 1) + "." + poll.options.get(c))
                                .setCallbackData(c + "_" + event));
                        rowsInline.add(rowInline);
                    }
                    markupInline.setKeyboard(rowsInline);
                    Iterator it = poll.groups.entrySet().iterator();
                    while (it.hasNext()) {
                        Map.Entry pair = (Map.Entry) it.next();
                        EditMessageText emt = new EditMessageText()
                                .setText(text)
                                .setChatId((Long) pair.getKey())
                                .setMessageId(poll.groups.get(pair.getKey()))
                                .setReplyMarkup(markupInline);
                        try {
                            bot.execute(emt);
                        } catch (Exception e) {
                        }
                    }
                } catch (Exception e) {
                }
                poll.semaphore.release();
            } catch (Exception e) {
            }
        } else {
            answerCallback(update, "Ocurrió un error.");
            EditMessageText emt = new EditMessageText()
                    .setText("Ocurrió un error inesperado: la encuesta finalizó o ya no existe.")
                    .setChatId(update.getCallbackQuery().getMessage().getChatId())
                    .setMessageId(update.getCallbackQuery().getMessage().getMessageId());
            try {
                bot.execute(emt);
            } catch (Exception e) {
            }
        }
    }

    public void shareEventMsg(Update update) {
        int id_poll = Integer.parseInt(update.getMessage().getText().split("_")[1]);
        Poll poll = null;
        try {
            bot.semaphore.acquire();
            if (bot.encuestas.containsKey(id_poll)) {
                poll = bot.encuestas.get(id_poll);
            }
            bot.semaphore.release();
        } catch (Exception e) {
        }
        if (poll != null) {
            try {
                poll.semaphore.acquire();
                int user_id = update.getMessage().getFrom().getId();
                if (user_id == poll.id_creator) {
                    if (poll.groups.containsKey(update.getMessage().getChatId())) {
                        EditMessageText emt = new EditMessageText()
                                .setText("La encuesta se cambió de mensaje.")
                                .setChatId(update.getMessage().getChatId())
                                .setMessageId(poll.groups.get(update.getMessage().getChatId()));
                        poll.groups.remove(update.getMessage().getChatId());
                        try {
                            bot.execute(emt);
                        } catch (Exception e) {
                        }
                    }
                    InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
                    List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
                    String text = poll.creator + " ha compartido una encuesta.\nNombre: "
                            + poll.name + "\nInterrogante: " + poll.question + "\nLista de opciones:\n";
                    for (int c = 0; c < poll.options.size(); c++) {
                        text += (c + 1) + "." + poll.options.get(c);
                        if (poll.limit_option_selection) {
                            text += " (" + poll.votos[c].size() + "/" + poll.limit_votes.get(c) + ")\n";
                        } else {
                            text += " (" + poll.votos[c].size() + ")\n";
                        }

                        List<InlineKeyboardButton> rowInline = new ArrayList<>();
                        rowInline.add(new InlineKeyboardButton()
                                .setText((c + 1) + "." + poll.options.get(c))
                                .setCallbackData(c + "_" + poll.id));
                        rowsInline.add(rowInline);

                    }
                    markupInline.setKeyboard(rowsInline);
                    SendMessage sm = new SendMessage()
                            .setChatId(update.getMessage().getChatId())
                            .setText(text).setReplyMarkup(markupInline);
                    try {
                        Message sms = bot.execute(sm);
                        poll.groups.put(update.getMessage().getChatId(), sms.getMessageId());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                poll.semaphore.release();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}
