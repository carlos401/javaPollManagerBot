/*
    ldapSyncBot
    Copyright (C) 2018  CrabTech
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License

    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.Semaphore;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardRemove;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;


public class PrivateBot {
    enum State {
        NoStateSet,
        HaskIfIsScheduleSelection,
        HaskLimitSelection,
        WaitingForName,
        ConfirmingName,
        WaitingForOptions,
        WaitingForQuestion,
        LimitDate,
        End,
        WaitForPollToStopAndShareResult

    }

    private Semaphore semaforo = new Semaphore(1);
    private Bot bot = null;
    private State state = State.NoStateSet;
    private Poll currentPoll = null;
    private int last_in_line_button = 0;

    public PrivateBot(Bot father) {
        bot = father;
        state = State.NoStateSet;
    }

    public void handle(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {
            String message = update.getMessage().getText();
            switch (message.split("@")[0]) {
                case "/start": {
                    start(update);
                    break;
                }
                case "/bye": {
                    bye(update);
                    break;
                }
                case "/viewpolls": {
                    viewPolls(update);
                    break;
                }
                case "/createpoll": {
                    try {
                        semaforo.acquire();
                        try {
                            createPoll(update);
                        } finally {
                            semaforo.release();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                }
                case "/ready": {
                    ready(update);
                    break;
                }
                case "/share": {
                    share(update);
                    break;
                }
                case "/about": {
                    about(update);
                    break;
                }
                case "/stopandsharepoll": {
                    stopAndSharePoll(update);
                    break;
                }
                case "/ayuda": {
                    help_msg(update);
                    break;
                }
                default: {
                    try {
                        semaforo.acquire();
                        try {
                            switch (state) {
                                case HaskIfIsScheduleSelection: {
                                    handleConfirmSchedule(update);
                                    break;
                                }
                                case HaskLimitSelection: {
                                    handleConfirmlimit(update);
                                    break;
                                }
                                case WaitingForName: {
                                    handleName(update);
                                    break;
                                }
                                case ConfirmingName: {
                                    handleConfirmName(update);

                                    break;
                                }
                                case WaitingForOptions: {
                                    handleOptions(update);

                                    break;
                                }
                                case WaitingForQuestion: {
                                    handleQuestion(update);

                                    break;
                                }
                                case LimitDate: {
                                    handleLimitDate(update);

                                    break;
                                }
                                case WaitForPollToStopAndShareResult: {
                                    handleForPollToStopAndShareResult(update);

                                    break;
                                }
                                default: {
                                    ununderstandableMessage(update);
                                    break;
                                }
                            }
                        } finally {
                            semaforo.release();
                        }
                    } catch (Exception e) {
                    }
                    break;
                }
            }
        }
    }

    public void createPoll(Update update) {
        currentPoll = new Poll(update.getMessage().getFrom().getId(),
                update.getMessage().getFrom().getFirstName() + " " + update.getMessage().getFrom().getLastName());
        currentPoll.chat_creation = update.getMessage().getChatId();
        state = State.HaskIfIsScheduleSelection;
        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        LinkedList<KeyboardRow> keyboard = new LinkedList<>();
        KeyboardRow row = new KeyboardRow();
        row.add("Si");
        row.add("No");
        keyboard.add(row);
        keyboardMarkup.setKeyboard(keyboard);
        send_message(update, "¿La encuesta es para organizar fechas?", keyboardMarkup);
    }

    public void handleConfirmSchedule(Update update) {
        if (update.getMessage().getText().equals("Si")) {
            currentPoll.is_schedule_poll = true;
            ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
            LinkedList<KeyboardRow> keyboard = new LinkedList<>();
            KeyboardRow row = new KeyboardRow();
            row.add("Si");
            row.add("No");
            keyboard.add(row);
            keyboardMarkup.setKeyboard(keyboard);
            send_message(update, "¿Cada opción tendra un límite de seleciones?", keyboardMarkup);
            state = State.HaskLimitSelection;
        } else {
            if (update.getMessage().getText().equals("No")) {
                currentPoll.is_schedule_poll = false;
                ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
                LinkedList<KeyboardRow> keyboard = new LinkedList<>();
                KeyboardRow row = new KeyboardRow();
                row.add("Si");
                row.add("No");
                keyboard.add(row);
                keyboardMarkup.setKeyboard(keyboard);
                send_message(update, "¿Cada opción tendrá un límite de seleciones?", keyboardMarkup);
                state = State.HaskLimitSelection;
            } else {
                ununderstandableMessage(update);
            }
        }
    }

    public void handleConfirmlimit(Update update) {
        if (update.getMessage().getText().equals("Si")) {
            currentPoll.limit_option_selection = true;
            currentPoll.limit_votes = new LinkedList<>();
            state = State.WaitingForName;
            send_message(update, "Ingrese un nombre para la encuesta:", null);
        } else {
            if (update.getMessage().getText().equals("No")) {
                currentPoll.limit_option_selection = false;
                state = State.WaitingForName;
                send_message(update, "Ingrese un nombre para la encuesta:", null);

            } else {
                ununderstandableMessage(update);
            }
        }
    }

    public void aksName(Update update) {
        currentPoll = new Poll(update.getMessage().getFrom().getId(),
                update.getMessage().getFrom().getFirstName() + " " + update.getMessage().getFrom().getLastName());
        state = State.WaitingForName;
        send_message(update, "Ingrese un nombre para la encuesta:", null);
    }

    public void send_message(Update update, String message, ReplyKeyboardMarkup keyboardMarkup) {
        if (keyboardMarkup == null) {
            SendMessage reply = new SendMessage()
                    .setChatId(update.getMessage().getChatId())
                    .setText(message).setReplyMarkup(new ReplyKeyboardRemove());
            try {
                bot.execute(reply);
            } catch (Exception e) {
                // e.printStackTrace();
            }

        } else {
            SendMessage reply = new SendMessage()
                    .setChatId(update.getMessage().getChatId())
                    .setText(message).setReplyMarkup(keyboardMarkup);
            try {
                last_in_line_button = ((Message) bot.execute(reply)).getMessageId();
            } catch (Exception e) {
                //   e.printStackTrace();
            }
        }
    }

    public void send_message_refer(Update update, String message, ReplyKeyboardMarkup keyboardMarkup) {
        if (keyboardMarkup == null) {
            SendMessage reply = new SendMessage()
                    .setChatId(update.getMessage().getChatId())
                    .setText(message).setReplyMarkup(new ReplyKeyboardRemove())
                    .setReplyToMessageId(update.getMessage().getMessageId());
            try {
                bot.execute(reply);
            } catch (Exception e) {
                //  e.printStackTrace();
            }

        } else {
            SendMessage reply = new SendMessage()
                    .setChatId(update.getMessage().getChatId())
                    .setText(message).setReplyMarkup(keyboardMarkup)
                    .setReplyToMessageId(update.getMessage().getMessageId());
            try {
                last_in_line_button = ((Message) bot.execute(reply)).getMessageId();
            } catch (Exception e) {
                //  e.printStackTrace();
            }
        }
    }

    public void send_message_handler(Update update, String message, InlineKeyboardMarkup inlineKeyboardMarkup) {

        SendMessage reply = new SendMessage()
                .setChatId(update.getMessage().getChatId())
                .setText(message).setReplyMarkup(inlineKeyboardMarkup);
        try {
            last_in_line_button = bot.execute(reply).getMessageId();
        } catch (Exception e) {

        }

    }

    public void handleName(Update update) {
        String message = update.getMessage().getText();
        currentPoll.name = message;
        state = State.ConfirmingName;
        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        LinkedList<KeyboardRow> keyboard = new LinkedList<>();
        KeyboardRow row = new KeyboardRow();
        row.add("Si");
        row.add("No");
        keyboard.add(row);
        keyboardMarkup.setKeyboard(keyboard);
        send_message_refer(update, "Conservar el nombre:", keyboardMarkup);
    }

    public void handleQuestion(Update update) {
        String message = update.getMessage().getText();
        currentPoll.question = message;
        state = State.WaitingForOptions;
        if (currentPoll.limit_option_selection) {
            send_message(update, "Ingrese las opciones junto al numero máximo de selecciones separados por un espacio.\nPara finalizar seleccione\n/FinalizarOpciones", null);
        } else {
            send_message(update, "Ingrese las opciones.\nPara finalizar seleccione\n/FinalizarOpciones", null);
        }
    }

    public void handleLimitDate(Update update) {
        int setLimit = currentPoll.setLimit(update.getMessage().getText());
        String errormessage = "";
        state = State.LimitDate;
        switch (setLimit) {
            case 1: {
                errormessage = "Error en el formato.";
                break;
            }
            case 2: {
                errormessage = "Hay una opción (fecha) que tiene una calendariazación menor al limite que intenta ingresar.";
                break;
            }
            case 3: {
                errormessage = "La fecha requiere al menos 10 minutos para ser contestada.";
                break;
            }
            case 4: {
                errormessage = "En la lista de opciones hay una fecha que ya no es coherente con el límite de 10 minutos, la operación se ha cancelado.";
                state = State.WaitingForName;
                break;
            }
            case 0: {
                state = State.End;
                //send_message(update, "Se ingreso " + currentPoll.limit.toString() + " como fecha limite.", null);
                end(update);
                break;
            }
        }
        if (setLimit != 0) {
            send_message(update, errormessage, null);
        }
    }

    public void handleConfirmName(Update update) {
        if (update.getMessage().getText().equals("Si")) {
            state = State.WaitingForQuestion;
            send_message(update, "Ingrese la interroggante de la encuesta", null);
        } else {
            if (update.getMessage().getText().equals("No")) {
                state = State.WaitingForName;
                send_message(update, "Reingrese un nombre para la encuesta", null);

            } else {
                ununderstandableMessage(update);
            }
        }

    }

    public InlineKeyboardMarkup InlineKeyboardMarkupForOption() {
        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
        for (int c = 0; c < currentPoll.options.size(); c++) {
            int x = 0;
            List<InlineKeyboardButton> rowInline = new ArrayList<>();
            String text = "";
            if (currentPoll.limit_option_selection) {
                text = " (" + currentPoll.limit_votes.get(c) + ")";
            }
            rowInline.add(new InlineKeyboardButton()
                    .setText((c + 1) + "." + currentPoll.options.get(c) + text + "❎")
                    .setCallbackData(currentPoll.options.get(c)));
            rowsInline.add(rowInline);
        }
        markupInline.setKeyboard(rowsInline);
        return markupInline;
    }

    public void handleOptions(Update update) {
        if (!update.getMessage().getText().equals("/FinalizarOpciones")) {
            String message = update.getMessage().getText();
            String reply_error = "";
            if (currentPoll.is_schedule_poll) {
                reply_error = valid_date(message);
            } else {
                if (currentPoll.limit_option_selection) {
                    if (message.split(" ").length <= 1) {
                        reply_error = "Error:\nDebe ingresar la opción junto a los votos maximos" +
                                " que puede tener, esto separado por un espacio.";
                    } else {
                        try {
                            int max = Integer.parseInt(message.split(" ")[message.split(" ").length - 1]);
                            if (max <= 0) {
                                reply_error = "Error:\nEl número mínimo de votos que podría tener una opción es 1";
                            } else {
                                String option = "";
                                for (int c = 0; c < message.split(" ").length - 1; c++) {
                                    if (c == 0) {
                                        option = option + message.split(" ")[0];
                                    } else {
                                        option = option + "" + message.split(" ")[0];
                                    }
                                }
                                if (!currentPoll.options.contains(option)) {
                                    currentPoll.options.add(option);
                                    currentPoll.limit_votes.add(max);

                                } else {
                                    reply_error = "Error:\nLa opción ya esta registrada.";
                                }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            reply_error = "Error:\nDebe ingresar la opción junto a los votos máximos" +
                                    " que puede tener, esto separado por una coma.";
                        }
                    }
                } else {
                    if (!currentPoll.options.contains(message)) {
                        currentPoll.add_options(message);
                    } else {
                        reply_error = "No se puede repetir opciones";
                    }
                }
            }
            if (!reply_error.equals("")) {
                send_message_refer(update, reply_error, null);
            }
            String reply = "";
            reply = "Ingrese las opciones de la encuesta.\n/FinalizarOpciones";

            if (currentPoll.limit_option_selection) {
                reply = "Ingrese las opciones de la encuesta junto al " +
                        "número máximo de votos que puede tener\n(Ejemplo: Opción 1)"
                        + ".\n/FinalizarOpciones";
            }
            send_message_handler(update,
                    reply,
                    InlineKeyboardMarkupForOption());
        } else {
            if (currentPoll.options.size() <= 0) {
                send_message_refer(update,
                        "Para finalizar las opciones requiere al menos agregar una.", null);
                send_message(update, "Se requiere al menos una opción. Ingrese las opiones\n/FinalizarOpciones\".", null);
            } else {
                state = State.LimitDate;
                send_message(update, "Ingrese una fecha límite:\n", null);
            }
        }
    }

    public void share(Update update) {
        send_message(update, "t.me/" + "GroupwareBot" + "?startgroup=shareevent_" + currentPoll.id, null);
    }

    public void handle_line_button(Update update) {
        if (currentPoll == null || last_in_line_button != update.getCallbackQuery().getMessage().getMessageId()) {
            EditMessageText emt = new EditMessageText()
                    .setMessageId(update.getCallbackQuery().getMessage().getMessageId())
                    .setChatId(update.getCallbackQuery().getMessage().getChatId())
                    .setText("El mensaje de control expiró o se encuentra más adelante.");
            try {
                bot.execute(emt);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (currentPoll.options.contains(update.getCallbackQuery().getData())) {
                if (currentPoll.limit_option_selection) {
                    currentPoll.limit_votes.remove(currentPoll.options.indexOf(update.getCallbackQuery().getData()));
                }
                currentPoll.options.remove(update.getCallbackQuery().getData());
            }
            EditMessageText emt = new EditMessageText()
                    .setText("\nInasdes.\n/FinalizarOpciones")
                    .setChatId(update.getCallbackQuery().getMessage().getChatId())
                    .setMessageId(update.getCallbackQuery().getMessage().getMessageId())
                    .setReplyMarkup(InlineKeyboardMarkupForOption());
            try {
                bot.execute(emt);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void deleteOption(Update update) {
        currentPoll.options.pop();
    }

    public void ready(Update update) {
        state = State.LimitDate;
        send_message(update, "Ingrese una fecha límite para contestar: \n(formato dd/mm/aaaa hh:mm)", null);
    }

    public void end(Update update) {
        currentPoll.votos = new LinkedList[currentPoll.options.size()];
        for (int c = 0; c < currentPoll.options.size(); c++) {
            currentPoll.votos[c] = new LinkedList<>();
        }
        currentPoll.id_creator = update.getMessage().getFrom().getId();

        String encuestas = "Nombre de la encuesta: " + currentPoll.name + "\nPregunta de la encuesta:"
                + currentPoll.question + "\nFecha límite: " + currentPoll.getStringLimit() + "\nOpciones:\n";

        String opciones = "\n";
        for (int c = 0; c < currentPoll.options.size(); c++) {

            opciones += "\n" + (c + 1) + "." + currentPoll.options.get(c);
            if (currentPoll.limit_option_selection) {
                opciones += "(" + currentPoll.limit_votes.get(c) + ")";
            }
        }
        encuestas += opciones;

        encuestas += "\nPara agregar esta encuesta a su calendario de google:\n"
        
                + "(https://script.google.com/macros/s/AKfycbx1CfuC3su_f6KvnSkemIjxLwuy_Na33bqhXSqi5mrjPUl63O6P/exec?name=" + currentPoll.name
                + "&date=" + currentPoll.getStringLimit()
                + "&description=" + currentPoll.question
                + ")\n";

        //encuestas += "\nPara compartir la encuesta en otros grupos:\nt.me/" + "GroupwareBot" + "?startgroup=shareevent_" + currentPoll.id;
        try {
            bot.semaphore.acquire();
            bot.count_polls = bot.count_polls + 1;
            currentPoll.id = bot.count_polls;
            bot.encuestas.put(currentPoll.id, currentPoll);
            bot.semaphore.release();
            send_message(update, encuestas, null);
            share(update);
        } catch (Exception e) {
            e.printStackTrace();
            send_message_refer(update, "Ha ocurrido un error", null);
        }


    }


    public void start(Update update) {
        send_message(update, "Has iniciado Poll Manager bot!\nIngrese /ayuda para ver opciones.", null);
    }

    public void bye(Update update) {
        send_message(update, "Hasta lueguito.", null);
    }

    public void viewPolls(Update update) {
        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        LinkedList<KeyboardRow> keyboard = new LinkedList<>();
        KeyboardRow row = new KeyboardRow();
        keyboard.add(row);
        keyboardMarkup.setKeyboard(keyboard);
        Iterator it = bot.encuestas.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            if (bot.encuestas.get(pair.getKey()).id_creator == update.getMessage().getFrom().getId()) {
                row.add(bot.encuestas.get(pair.getKey()).id + "");
            }
        }
        send_message(update, "Seleccione la encuesta que desea ver:", null);
    }

    public void about(Update update) {
        send_message(update, "Info", null);
    }

    public void stopAndSharePoll(Update update) {
        String text = "Indique una encuesta:\n";
        boolean has_polls = false;
        Iterator it = bot.encuestas.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            if (bot.encuestas.get(pair.getKey()).id_creator == update.getMessage().getFrom().getId()) {
                text += "\nNombre:" + bot.encuestas.get(pair.getKey()).name + "\n/StopAndShare" + bot.encuestas.get(pair.getKey()).id;
            }
        }
        if (!has_polls) {
            text = "No has creado encuestas aún.";
        } else {
            send_message(update, text, null);
            state = State.WaitForPollToStopAndShareResult;
        }
    }

    public void handleForPollToStopAndShareResult(Update update) {

    }

    public String valid_date(String date) {
        String pattern = "^[0-9]{1,2}\\/[0-9]{1,2}\\/[0-9]{1,4} (?:(?:2[0-3])|(?:[0-1]?[0-9])):(?:[0-5]?[0-9])";
        GregorianCalendar fecha = null;
        String error = "";
        if (currentPoll.limit_option_selection) {
            pattern += " \\d+$";
        } else {
            pattern += "$";
        }
        if (date.matches(pattern)) {
            int day = Integer.parseInt(date.split(" ")[0].split("/")[0]);
            int month = Integer.parseInt(date.split(" ")[0].split("/")[1]);
            int year = Integer.parseInt(date.split(" ")[0].split("/")[2]);
            int hour = Integer.parseInt(date.split(" ")[1].split(":")[0]);
            int minute = Integer.parseInt(date.split(" ")[1].split(":")[1]);
            try {
                fecha = new GregorianCalendar(year, month - 1, day, hour, minute);
            } catch (Exception e) {
                error = "El formato no es válido.";
            }
            GregorianCalendar compare = (GregorianCalendar) GregorianCalendar.getInstance();
            compare.add(Calendar.MINUTE, 15);
            if (!(error.equals("") && fecha.compareTo(compare) >= 0)) {
                error = "La fecha de expiración es muy cercana, necesita ser mayor a quince minutos.";
            }
            if (error.equals("")) {
                if (currentPoll.limit_option_selection) {
                    SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                    if (currentPoll.options.contains(fmt.format(fecha.getTime()))) {
                        error = "La fecha ya existe.";
                    } else {
                        try {
                            int x = Integer.parseInt(date.split(
                                    "^[0-9]{1,2}\\/[0-9]{1,2}\\/[0-9]{1,4} (?:(?:2[0-3])|(?:[0-1]?[0-9])):(?:[0-5]?[0-9]) ")[1]);
                            if (x < 0) {
                                error = "Se requiere un número entero.";
                            } else {
                                currentPoll.options.add(date.split(" \\d+$")[0]);
                                currentPoll.limit_votes.add(x);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            error = "asdas";
                        }
                    }
                } else {
                    if (error.equals("")) {
                    } else {
                        SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                        if (currentPoll.options.contains(fmt.format(fecha.getTime()))) {
                            error = "La fecha ya existe.";
                        } else {
                            currentPoll.options.add(date);
                        }
                    }
                }
            }
        } else {
            error = "Hay un error en el formato.";
        }
        return error;
    }

    public void ununderstandableMessage(Update update) {
        send_message_refer(update, "No entendí ese último mensaje.", null);
    }

    public void help_msg(Update update) {
        send_message_refer(update, "Ayuda.", null);
    }
}