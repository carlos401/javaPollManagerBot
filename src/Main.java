/*
    ldapSyncBot
    Copyright (C) 2018  CrabTech

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import org.glassfish.grizzly.utils.EchoFilter;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.exceptions.TelegramApiRequestException;

import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Map;

public class Main {
    static class FinishPolls implements Runnable {
        Bot bot = null;

        FinishPolls(Bot father) {
            bot = father;
        }

        @Override
        public void run() {
            while (true) {
                try {
                    bot.semaphore.acquire();
                    Iterator it=null;
                    try {
                        it = bot.encuestas.entrySet().iterator();
                    }finally {
                        bot.semaphore.release();
                    }
                    while (it!=null&&it.hasNext()) {
                        Map.Entry pair = (Map.Entry) it.next();
                        Poll poll = (Poll) pair.getValue();
                        Iterator grupos = poll.groups.entrySet().iterator();
                        if (!poll.finished) {
                            GregorianCalendar compare = (GregorianCalendar) GregorianCalendar.getInstance();
                            if (poll.limit.compareTo(compare) < 1) {
                                poll.finished = true;
                                String ganador = "";
                                if (!poll.limit_option_selection) {
                                    int count_max = 0;
                                    int max = 0;
                                    for (int c = 0; c < poll.options.size(); c++) {
                                        if (poll.votos[c].size() > count_max) {
                                            max = c;
                                            count_max = poll.votos[c].size();
                                        }
                                    }
                                    if (count_max == 0) {
                                        ganador = "No hubo una opción ganadora en la encuesta";
                                    } else {
                                        ganador = "La opcion que tuvo mas votos fue\n" + poll.options.get(max) + "\nCon "
                                                + count_max + " votos.";
                                    }
                                } else {
                                    boolean have_votes = false;
                                    ganador = "\nLas seleciones fueron:\n";

                                    for (int c = 0; c < poll.options.size(); c++) {
                                        if (poll.votos[c].size() > 0) {
                                            have_votes = true;
                                            ganador += "\n"+(c+1)+". " + poll.options.get(c);
                                            for (int x = 0; x < poll.votos[c].size(); x++) {
                                                ganador += "\n--" + bot.usuarios.get(poll.votos[c].get(x));
                                            }
                                            ganador+="\n";
                                        }
                                    }
                                    if (!have_votes) {
                                        ganador = "No hubo ningún registro de votos";
                                    }
                                }
                                SendMessage sm_1 = new SendMessage()
                                        .setText("La encuesta número:" + poll.id + "\nNombre: " + poll.name + "\nHa finalizado." + ganador)
                                        .setChatId(poll.chat_creation);
                                try {
                                    bot.execute(sm_1);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                while (grupos.hasNext()) {
                                    Map.Entry pair_group = (Map.Entry) grupos.next();
                                    SendMessage sm = new SendMessage()
                                            .setText("La encuesta número:" + poll.id + "\nNombre: " + poll.name + "\nHa finalizado." + ganador)
                                            .setChatId((Long) pair_group.getKey());
                                    try {
                                        bot.execute(sm);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    }
                    Thread.sleep(600000 / 10);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
    }
    public static void main(String arsg[])
    {
        ApiContextInitializer.init();
        TelegramBotsApi tba = new TelegramBotsApi();
        Bot bot = new Bot();
        try
        {
            Thread mThread= new Thread(new FinishPolls(bot));
            
            mThread.start();
            
            tba.registerBot(bot);

        } catch (TelegramApiRequestException e) {
            e.printStackTrace();
        }
    }
}
