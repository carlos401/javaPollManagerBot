# We take advantage of the potential of Telegram
Our app use the advantages that Telegram offers. Telegram is:
* Private.
* Cloud based. 
* Fast
* Distributed
* Open
* Free
* Secure
* Powerfull

Telegram has a native app for every platform:
* Web version
* macOS
* Windows
* Linux

And it is used on mobile OS:
* Android
* iPhone / iPad
* Windows Phone

To learn more about Telegram, please visit: https://telegram.org/.

---

# Clear documentation for dummies
## I'm a Telegram user
In order to use the Bot, follow this steps:
1. Open your *saved messages* chat
2. Type the next command and click it

    **@GroupWareBot**

3. Then press the *start* button and use the Bot

For more information, read this [guide]()

## I'm a developer
In order to contributed to this repo, follow this steps:
1. Fork this project
2. Create a new branch from *master*
3. Developt and push your changes
4. Create a merge request

For more information, send a mail to: <geovanny.corderovalverde@ucr.ac.cr>

---

# ¡We are Open Source!

    groupWareBot
    Copyright (C) 2018  CrabTech

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.